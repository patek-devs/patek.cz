---
title: "Homepage"
date: 2019-09-08T11:10:18+02:00
authors: [ "Greenscreener" ]
---
We are a group of students interested not only in IT and programming but also physics, electrical engineering, amateur radio, math, biology and anything interesting in general.

<br>

<span style="display: inline-block; padding-top: .5em; padding-bottom: .5em;">Interested in what we do?</span>   <a href="{{% relref  "/join-us" %}} " class="button is-large"> Join us! </a>

<br>

[Stránky v Češtině 🇨🇿]({{% relref path="." lang="cs" %}})
