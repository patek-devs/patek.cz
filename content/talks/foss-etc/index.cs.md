---
title: "Openness, the user freedoms, and the business"
date: 2020-09-07T14:13:37+02:00
authors: [ "vojta001" ]
when: 2020-05-25T15:45:00+02:00
slides:
  - type: resource
    data: presentation.pdf
recordings:
  - data: https://www.youtube.com/watch?v=-4XEerftX-o
---

Tentokrát se jednalo spíše o společné zamyšlení a výzvu, než o klasickou informativní přednášku. Mluvil jsem o tom, co vlastně všechno může být otevřené a hlavně, k čemu je to vlastně dobré. Taky jsme se zamysleli, jak můžou otevřenost uchopit vývojáři, aby naplnili naše uživatelské svobody a stále ještě vydělali nějaké peníze. Nechyběla ani ukázka, jak snadné je dnes přispět do open source software a že to nemusí dělat jen akademici. Každý, byť sebemenčí, kousek se počítá.
