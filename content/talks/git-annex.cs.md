---
title: "git-annex"
date: 2021-05-24T12:04:27+02:00
authors: [ "vojta001" ]
when: 2021-01-29T15:00:00+01:00
---

[O Gitu samotném jsme již nedávno mluvili]({{< relref "/talks/git" >}}). Co když jsme jeho kouzlu propadli a teď bychom ho rádi použili i na velké binární soubory, jako sestavené spustitelné soubory, zálohy, nebo třeba rodinné fotografie? Pro první zmíněné použití se výborně hodí [Git LFS](https://git-lfs.github.com), ale u dalších bychom mohli chtít sledovat, na kterých strojích jsou soubory uložené, zda-li splňují požadavky na replikaci či podporu pro cloudová úložiště a šifrování.

Zde přichází na scénu [git-annex](https://git-annex.branchable.com) – open source rozšíření Gitu napsané v Haskellu, které zavádí tzv. _location tracking_, tedy pamatuje si, kde jsou jednotlivé soubory uložené. Díky tomu se výborně hodí pro zálohování a archivování velkých souborů. Též má zmíněnou podporu pro různá cloudová úložiště včetně šifrování.
