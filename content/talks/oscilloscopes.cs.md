---
title: "Osciloskopy"
date: 2020-02-23T12:07:53+01:00
authors: [ "HonzaSixta" ]
when: 2020-02-21T14:00:00+01:00
---
Osciloskop je jedním ze základních nástrojů elektrotechnika, který má mnoho využití. Co to je osciloskop, jak funguje, co se s ním dá dělat a mnoho dalšího nám včetně praktických ukázek přišel zodpovědět člen [týmu eForce](https://eforce.cvut.cz) a bývalý student gymnázia Honza Sixta.
