---
title: "CNC1 - Práce s frézkou"
date: 2023-10-16T16:30:00+02:00
authors: [ "Glassknight" ]
when: 2023-10-20T15:00:00+02:00
slides:
recordings:
---

Na Pátku 20. 10. plánuju zaučit lidi v práci s CNC frézkou.

Je těžké pro začátečníky najít dobrý software a zorientovat se v něm. Dnes se proto zaměřím na můj workflow při výrobě plošných spojů.

Někdy v budoucnu plánuju pokračovat přednáškami o 3D frézování a vyšívání.

PS. Nevím, kdy přesně přijedu, ale někdy okolo 15h

PPS. Software který budu v pátek využívat je KiCad, FlatCAM, a Candle
