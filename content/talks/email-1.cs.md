---
title: "Základní koncepty emailu"
date: 2023-09-21T23:58:38+02:00
authors: [ "vojta001" ]
when: 2023-09-22T15:00:00+02:00
slides:
recordings:
---

Na Pátku 22.9. plánuju povědět spíše kratší úvodní přednášku o tom, jak doopravdy vypadá email a co to pro nás má za důsledky. Bude-li zájem, navážu v budoucnu podrobnějším povídáním, v němž se všemu podíváme patřičně pod pokličku.

Očekávám, že se podíváme na strukturu emailové zprávy, na to, jak se emaily směrují a uvědomíme si, že to vůbec nesouvisí s provozováním emailové schránky (tedy že mailbox a SMTP server jsou nezávislé komponenty).
