---
title: ">install gentoo"
date: 2020-10-15
authors: [ "nixi" ]
when: 2020-10-23T16:00:00+02:00
recordings:
  - data: https://www.youtube.com/watch?v=KC_QrgJ8mBo
---

Ve světě unixových operačních systémů existuje spousta možností, hlavně co se týče Linuxových distribucí (Yes Richard, [I know](https://pastebin.com/raw/jtRBT5F1) and I [don't care](https://pastebin.com/raw/QD4q7r5P)). Mezi nejoblíbenější distribuce patří Arch Linux, rádoby *minimalistická* hračka těch, kteří rádi machrují na lidi s Ubuntu, ale sami nemají na to, aby si nakonfigurovali svůj kernel a nainstalovali Gentoo.

[Gentoo](https://gentoo.org/) je jedna z mála takzvaných *source-based* distribucí, jejichž principem je, že software se neinstaluje v binární podobě, nýbrž že veškerý software se kompiluje ze zdrojového kódu přímo pro dané zařízení. Uživatel má možnost přesně nastavit, s jakými možnostmi a optimalizacemi bude každý kus softwaru v systému fungovat. Celý systém je pak optimalizovaný přímo na daný procesor a pro daného uživatele. Firefox bez PulseAudia? No problem! Gentoo je zároveň jméno pro nejrychleji plavajícího tučňáka na světe, což je metafora pro rychlý optimalizovaný systém :)

V přednášce představím samotnou distribuci, její principy, Portage, Genkernel a vysvětlím, jak probíhá základní konfigurace pomocí flagů a klíčových slov. Takže `doas emerge -avuND @world` a [get ready to compile](https://invidious.tube/watch?v=VjGSMUep6_4).

![Papa Francesco](papa-francesco.jpg)

