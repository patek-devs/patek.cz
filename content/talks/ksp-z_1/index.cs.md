---
title: "Úvod do programování v Pythonu a KSP-Z"
date: 2020-09-27T21:21:17+02:00
authors: [ "vojta001" ]
when: 2020-09-25T14:30:00+02:00
slides:
  - type: resource
    data: 20200925_175815.jpg
    desc: Tabule s vysvětlením Pythonu
  - type: resource
    data: 20200925_175828.jpg
    desc: Tabule s pseudokódem k 29-Z1-1
  - type: resource
    data: 29-Z1-1.py
    desc: Komentovaný zdrojový kód
---

Tento Pátek Talk proběhl spíše formou interaktivního workshopu, nežli frontální přednášky.

Popovídali jsme si nejprve, jak počítače fungují uvnitř a jak se programují. Přes přehled programovacích jazyků a jejich vlastností jsme se dostali až k Pythonu, na němž jsme prozkoumali syntaxi, základní datové typy i nejčastější řídící konstrukty. Pro zájem došlo i na rychlé porovnání s C, ale bohužel nezbyl čas toho využít.

Poté následovalo samotné řešení. Vybral jsem úlohu ze začátečnické kategorie [Korespondenčního Semináře z Programování](https://ksp.mff.cuni.cz) (KSP), který pořádá [Matematicko-fyzikální fakulta Univezity Karlovy](https://mff.cuni.cz), konkrétně _[29-Z1-1 Kevinova želva](https://ksp.mff.cuni.cz/z/ulohy/29/zadani1.html#task1)_. Úloha je již dávno po termínu odevzdání, díky tomu nevadilo, že jsme na její řešení přišli společně a nakonec ji i „nanečisto“ odevzdali. Nejprve jsme si společnými silami rozmysleli algoritmus na papíře v tzv. pseudokódu a poté implementovali.

Pro zájemce přikládám fotky tabule i můj zdrojový kód, ale obávám se, že pomůžou jen těm, kteří se zůčastnili úvodního povídání.
