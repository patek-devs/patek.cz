N = int(input()) # N nepotřebujeme, ale přesto ho musíme načíst
P = input() # příkazy

# výchozí souřadnice želvy
x = 0
y = 0

# projdeme všechny příkazy a aktuální si vždy pojmenujeme p
for p in P:
    if p == "S": # pokud je příkaz S
        y += 1 # aktualizujeme příslušnou souřadnici
    elif p == "J": # jinak pokud (proto elif) je příkaz J
        y -= 1
    elif p == "V":
        x += 1
    elif p == "Z":
        x -= 1

print(str(x) + " " + str(y)) # vypíšeme finální souřadnice oddělené mezerou
