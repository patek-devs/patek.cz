---
title: "Vojta"
fullname: "Vojta Káně"
---
Jeden ze zakládajících členů Pátku nadšený pro vše informatické i zcela normální.

Dnes již bohužel na GBL nestuduju, přesto se snažím udržet v centru dění. A taky rád do všeho kecám.

Více o mně na [mém webu](https://vkane.cz).
