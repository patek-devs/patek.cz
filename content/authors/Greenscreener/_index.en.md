---
title: "Greenscreener"
avatar: "https://grsc.cz/images/GSLOGO.svg"
fullname: "Jan Černohorský"
---
Hi, I'm Greenscreener or Jan Černohorský, if you prefer my real name. I'm a founding member of Pátek. I am one of the authors of this website and also gbl.cz. I am a keen user of linux and I spend most of my time making web stuff. To my own disgust, the language I'm best at is still JavaScript, but I also write in Python, bash and similar. I really like Go, but I haven't found the time yet to sit down and properly learn it. I participate in the Czech Cybersecurity Contest, play CTFs and sometimes hack stuff. I like playing with Arduino and other electronics, I often do some 3D printing at school. Sometimes I even make stuff in Blender.

You can find me on [Twitter](https://twitter.com/GrnScrnr), [GitHub](https://github.com/Greenscreener), [Telegram](https://t.me/grnscrnr) and find out more about me on my website [grsc.cz](https://grsc.cz).
