---
title: "Úvodní stránka"
date: 2019-09-08T11:10:15+02:00
authors: [ "Greenscreener" ]
---
Pátek je kroužek a skupina studentů, kteří se zajímají nejen o informatiku, ale také fyziku, elektrotechniku, radioamatérství, matematiku, biologii a vše, co je zajímavé.

<br>

<span style="display: inline-block; padding-top: .5em; padding-bottom: .5em;">Zaujalo tě, co děláme?</span>   <a href="{{% relref  "/join-us" %}} " class="button is-large"> Přidej se k nám! </a>

<br>

[English homepage 🇬🇧]({{% relref path="." lang="en" %}})
