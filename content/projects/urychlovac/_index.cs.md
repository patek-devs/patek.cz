---
title: "Projekt Urychlovač"
authors: [ "Greenscreener", "sijisu" ]
leader: [ "sijisu" ]
---
Chtěli jsme jet na MakerFaire, ale neměli jsme co prezentovat. Tak jsme za měsíc postavili urychlovač částic.

Repozitář s tím, co z toho vzniklo, najdete zde: [https://gitlab.com/patek-devs/2019-mfp/particle-accelerator](https://gitlab.com/patek-devs/2019-mfp/particle-accelerator)

Pokud vidíte tento text, znamená to, že se ještě nikdo nepřiměl k podrobnému popsání realizace projektu. Pokud si myslíte, že to je škoda, a chcete vědět více informací, můžete prozatím vyjádřit svůj hněv na [patek@gbl.cz](mailto:patek@gbl.cz), popřípadě s napětím vyčkávat až se k tomu pátečníci dokopou.
