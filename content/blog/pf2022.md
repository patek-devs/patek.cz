---
title: "PF 2022"
date: 2022-01-13T19:11:07+01:00
authors: [ "vojta001" ]
tags: [ ]
---

Nový rok zrodil se za záře ohňostrojů již nějaký ten Pátek zpět, ale nikdo zatím PF nesepsal. A protože kdo se nepochválí sám, toho nepochválí nikdo, jdu na to!

Dvoutisící dvacátý první rok Páně nesl se ve znamení sklizně. Dlouhá úsilí začala nést ovoce. Za dosluhující školní server jsme nejen vybrali náhradu, ale rovnou se v užším kruhu stali jeho správci (tímto vyzývám každého,kdo by se chtěl podílet, nebo jen nahlédnout pod pokličku, ať se přihlásí).

Úspěšné bylo i sebevzdělávání. Pomalu ale přec se obnovuje nabídka i poptávka přednášek, roste i zájem jednotlivců naučit se programovat. A jako produkt přednášek minulých, profilují se nám pravděpodobní budoucí lídři Pátku. Aktuálně se rozrostl tým 3D tiskařů a správce to nejspíš za chvíli čeká taky. A abychom nezůstali jen u vnitřních záležitostí: výrazně narostl počet pátečních řešitelů [Korespondenčního Semináře z Programování](https://ksp.mff.cuni.cz) a na organizaci finále [evropské výzvy v kyberbezpečnosti](https://ecsc.eu) se naši členové v [roce 2021](https://ecsc2021.cz) také nemale podíleli.

Více takových úspěšných let!

Vojta
