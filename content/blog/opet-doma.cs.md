---
title: "První Pátek tohoto školního roku, konečně „doma“"
date: 2021-09-09T21:50:00+02:00
authors: [ "Greenscreener" ]
tags: [ "schuze" ]
---

Po prázdninách a dlouhé kovidové odmlce před nimi jsme byli všichni velmi
natěšení na opětovné setkání na Pátku. Jako obvykle jsme přizvali studenty z primy a z 1.A a také jich několik dorazilo. Ukázali jsme jim naše prostory a vybavení, včetně tradičně oblíbené 3D tiskárny. Někteří z primánků i zůstali a něco si i vytiskli.

Velkou změnou ale bylo, že schůze probíhala, poprvé po několika letech, v laboratořích fyziky. V laborkách se Pátek zrodil a dlouho tam fungoval, dokud nezačala jejich rekonstrukce, kvůli které jsme se museli uchýlit do daleko menšího videosálku. Ten jsme si za tu dobu také zamilovali -- a pořádně se v něm zabydleli (čtěte: je tam bordel jak v tanku), ale hned jak jsme se ocitli zpět v laborkách, probudily se v nás vzpomínky a naše setkání mělo opět tu skvělou atmosféru, která se v malém videosálku prostě nedala navodit.

Také jsme nějaký ten čas věnovali přípravě na nadcházející [MakerFaire v Praze](https://prague.makerfaire.com/), kde jsme [přihlášení jako vystavovatelé](https://prague.makerfaire.com/maker/entry/1071/).

