---
title: "Páteční šachový turnaj - PŠT"
date: 2021-02-05T18:00:00+01:00
authors: [ "Franta" ]
tags: [ ]
---
Epidemiologická situace nám stále nepřeje, a tak zvesela pořádáme Pátky ve
virtuálním prostoru. Abychom si chvíli odpočinuli a nabrali energii,
zorganizovali jsme si malý amatérský šachový turnaj! Použili jsme jednoduchý
švýcarský systém, abychom si co nejvíce užili hry. Vítězem je protentokrát **Petr**
z microlabu. Soudě dle nadšení mezi Pátečníky se dá očekávat, že se akce bude
někdy v budoucnu opakovat. Pokud mezi nás do té doby zavítáte, budeme moc rádi,
když se zúčastníte.

**Pátku zdar!**
