---
title: "COVID – část druhá"
date: 2020-10-06T21:59:34+02:00
authors: [ "vojta001", "Greenscreener" ]
tags: [ ]
---
Po neuvěřitelném zahájení školního roku plném nových skvělých lidí, workshopu 3D tisku, návštěvy jednoho z našich nejstarších členů, či [rychloprojektu generování Morseovky Arduinem](https://gitlab.com/patek-devs/2020-08-18-morse-arduino) přichází tvrdý střet s realitou.

Protože se epidemiologická situace opět stává nepříznivou, naši středoškolští členové jsou na distanční výuce a byla by jen otázka času, než by na Pátek přišel někdo pozitivní, nezbývá našim svědomím, než s velkým zármutkem **přerušit naše milovaná setkání ve fyzickém světě až do odvolání**. Tedy už ani **<time datetime="2020-10-09">Pátek 9. 10.</time> prezenčně nebude**. Doufáme, že budeme moct toto drastické opatření co nejdříve odvolat, ale prozatím jej prosím přijměte s pochopením. **Distanční program samozřejmě bude**, ale bude přístupný nejen lidem, kteří se viru nebojí, ale i těm, kteří jsou opatrnější, třeba protože žijí v jednom domě s prarodiči.

Teď přeci jen něco z pozitivnějšího soudku. Chtěli bychom ještě jednou vyzvednout [Páteční Streamované Přednášky]({{< relref "/tags/psp" >}}) z <time datetime="2020-05">května tohoto roku</time> a ještě jednou poděkovat všem přednášejícím, kteří se na nich podíleli. Určitě v nich budeme pokračovat; máme ještě nějaká témata v zásobě z minula a další se vymýšlejí včetně různých odvážnějších konceptů jako distanční HW bastlení a mnoho dalšího. A doufáme, že my všichni (tedy i **vy**) **vymyslíme další skvělé varianty distančních aktivit**, ať už **vzdělávacích**, **seznamovacích**, nebo prostě jen **zábavných** a **hracích**, protože o tom všem Pátek je a všechny nápady **rádi podpoříme**.

Co si tedy odnést závěrem? **Pátek nekončí**, ba naopak **je v plné síle a vy můžete být při tom**. Též **<time datetime="2020-10-09">Pátek 9. 10.</time> není zrušen, jen bude distančně**; podrobnosti zveřejníme obratem.

**Pátku zdar!**
