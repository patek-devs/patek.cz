---
title: "Trocha historie Pátku, jak Pátek k doméně přišel a proč už ji nemáme"
date: 2024-01-07T01:49:51+01:00
authors: [ "Greenscreener" ]
tags: []
---

Psal se rok 2019 a skupinka studentů brandýského gymnázia si teprve nedávno
začala říkat Pátek. Tehdy to bylo jen pár měsíců od chvíle, kdy se Páteční
mailová konfera přejmenovala ze studentskyweb@gbl.cz a všechny flejmy, věty o
šedesáti slovech a všechna komunikace mezi Pátečníky probíhaly právě po mailech.
Byl to také necelý rok od doby, kdy Pátečníci, kteří tehdy na protest proti
nízké kvalitě školního webu provozovali vlastní alternativu
[našegbl.cz](http://web.archive.org/web/20180730212117/https://nasegbl.cz/),
byli osloveni se slovy „Tak si to udělejte sami, když to umíte líp…“, aby
spravovali oficiální web školy [gbl.cz](https://gbl.cz). Také to byl necelý
půlrok od chvíle, kdy škola dostala první 3D tiskárnu a svěřila ji právě do
rukou Pátečníku. Byla to doba velkých plánů a ještě většího nadšení, psaní
stanov spolku, plánování rozpočtu a spousty projektů, ale taky doba, kdy si
Pátek pro sebe chtěl pořídit nějaké jméno a identitu.

Tehdy jsem, inspirován myšlenkou za [logem
CESNETu](https://www.cesnet.cz/sdruzeni/logo-cesnet), vytvořil logo Pátku, kde
třičtvrtěkružnice svou přítomností či nepřítomností kódují v binární soustavě
XOR písmen ve slově Patek. To, co dnes Pátek používá, je opravdu ten první
návrh, který jsem tehdy za pár minut slepil v inkscapu. Také tehdy proběhl
historický flejm o podobě triček, kdy se prosadila trička ve stylu „ironman“ se
soustřednými třičtvrtěkružnicemi na hrudi, v, dnes již ikonické, námořně
petrolejové modré barvě, kterou Pátek používá právě od té doby.

Když už jsme si začínali velmi zřetelně vyhrazovat své místo na světě,
samozřejmě jsme chtěli také mít svoje webové stánky. Nojo, už se k té doméně
skoro dostávám. 😁 Hledali jsme, kam naše stránky dáme. Říkali jsme si, že něco
krátkého, jako patek.tld, by bylo super. Samozřejmě, najít takovou doménu není
jednoduché. Půlku těch evropských vlastnila jistá hodinářská firma, ostatní byly
taky zabrané a po pár dnech uvažování jsme nakonec skončili u patek.dev. Tou
dobou jsme také už byli přihlášení na náš první
[MakerFaire](https://makerfaire.cz/praha) (i přesto, že jsme jako informatici
neměli moc co ukazovat, ale to je na jiné vyprávění) a jak jsme tak procházeli
WHOIS záznamy, všimli jsme si, že patek.cz vlastní Alan Fabik, o kom jsme
zjistili, že je CEO společnosti BigClown, která byla tehdy bronzovým sponzorem
MakerFairu, a samozřejmě jsme začali fantazírovat. „Tak jestli jsou bronzovej
sponzor, tak tam určitě budou,“ „Hehe, tak za ním prostě přijdem a zeptáme se
ho, jestli nám doménu nedá ne? 😁“

MakerFaire se pomalu přiblížil, doménu jsme si zatím žádnou nepořídili, protože
jsme měli plné ruce s přípravami, a ani jsme se nenadáli, už jsme, opatřeni
tričky s logem a nápisem Pátek na zádech, ukazovali spoustu rychle zbastlených
projektů kolemjdoucím a nasávali neskutečně živou atmosféru všude okolo. Když
jsem se v sobotu k odpoledni rozhodl dát si přestávku a projít se po výstavišti,
zrovna jsem někomu vyprávěl: „Hele, tady má stánek BigClown, od nich je ten
týpek co má patek.cz,“ než jsem stihl tuhle větu doříct, Alana jsem zahlédl a
tak jsem se rychle vydal zpět za ostatními, že když tu je, tak to přeci musíme
zkusit. Vyrazili jsme v docela početné skupince k jejich stánku, kde jsme se
začali handrkovat, kdo že na Alana promluví, k čemuž se nikdo z nás,
introvertních informatiků, moc neměl. Tak dlouho jsme na něj zírali, až nás
oslovil sám, jestli nám může nějak pomoct. Pak se to stalo ještě rychleji, než
jsme vůbec čekali. V první větě nám řekl, jaká je to náhoda, že si nás všiml v
seznamu vystavujících, protože vlastní doménu s naším jménem a nemá pro ni
využití. Ve třetí větě nám nabídl tykání a v páté, že nám doménu půjčí.

Tenhle neuvěřitelně znějící příběh má však velmi prozaický konec. Alan nám
krátce po svátcích napsal, že už s doménou plány má a že ji chce zpátky. To
nás svým způsobem přivedlo zpět ke kořenům, protože se po opravdu dlouhé době
strhla intenzivní diskuse v mailové konfeře, která po vzniku Pátečního Telegramu
zela prázdnotou. Nakonec jsme zorganizovali hovor na Jitsi, kde jsme zvážili
několik variant, až jsme se nakonec rozhodli pro koupi domény patekvpatek.cz a
začli plánovat přesun, součástí kterého bylo i napsání tohohle článku.

Tímto Alanovi ještě jednou děkujeme za zapůjčení domény a přejeme jemu i doméně
patek.cz šťastné pokračování cesty bez nás.
