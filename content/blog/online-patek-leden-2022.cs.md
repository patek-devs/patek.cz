---
title: "Online Pátek 21. 1. 2022"
date: 2022-01-20T11:41:09+01:00
authors: [ "Greenscreener" ]
tags: []
---
Zdravím všechny Pátku spřízněné!

Byť je to škoda, rozhodli jsme, že se budeme muset, minimálně tento týden,
přesunout Pátek zpět do **online prostoru**. Důvodem je hlavně poněkud
alarmující šíření covidu a s ním související karantény na naší škole.

Sejdeme se jako obvykle odpoledne, ale tentokrát na Gatheru. Jedná se o
platformu, která by nám měla umožnit alespoň částečně emulovat Páteční
atmosféru.

https://gather.town/app/dkDXAkIPO9S86EZD/patek

Mezi plánované body programu patří společné vyřešení pár úloh v picoCTF mini,
případně, jako obvykle, o co bude mít kdo zájem.
